import axios from 'axios';
// import { Platform } from 'react-native';

// const localURL = Platform.OS === 'android' ? '10.0.3.2' : 'localhost'
// const localURL = '10.10.20.215'
// 'http://' + localURL + '/db_padrao',

// export const ioURL = 'http://10.0.3.2:4000'
export const ioURL = 'https://socket-cadastros.herokuapp.com'

export const api = axios.create({
    baseURL: ioURL + '/api'
});