import Realm from 'realm'
import CadastrosSchema from '../schemas/CadastrosSchema'

export default function getRealm() {
    return Realm.open({
        deleteRealmIfMigrationNeeded: true,
        schema: [CadastrosSchema]
    })
}