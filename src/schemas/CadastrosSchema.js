export default class CadatrosSchema {
    static schema = {
        name: 'Cadastros',
        primaryKey: 'cpf',
        properties: {
            id: { type: 'string', indexed: true },
            cpf: 'string',
            nome: 'string',
            dataCadastro: { type: 'date', default: new Date() },
            salvar: { type: 'bool', default: false },
            excluir: { type: 'bool', default: false }
        }
    }
}