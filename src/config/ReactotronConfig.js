import Reactotron from 'reactotron-react-native';
import { reactotronRedux } from 'reactotron-redux';
import sagaPlugin from 'reactotron-redux-saga';

const reactotron = Reactotron.configure({ host: '10.10.20.215', name: 'Conectando ao Reactotron' })
    .useReactNative()
    .use(reactotronRedux())
    .use(sagaPlugin())
    .connect();

Reactotron.warn('Iniciando monitoramento da aplicação')

export default reactotron