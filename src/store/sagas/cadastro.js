import { all, call, put } from 'redux-saga/effects'
import { api } from '../../services/api'
import Reactotron from 'reactotron-react-native';

import getRealm from '../../services/realm';
import CadastrosActions from '../ducks/cadastros'

export function* refreshCadastrosSocket() {
    Reactotron.log("refreshCadastrosSocket")
    const realm = yield getRealm()
    const colCadastros = realm.objects('Cadastros').filtered(" salvar = false AND excluir = false ")

    let data = { cpfs: [] }

    colCadastros.map((cadAtual) => {
        data.cpfs.push(cadAtual.cpf)
    })

    try {
        yield call(api.post, '/cadastros/refresh', data)
    } catch (e) { }

    yield put(CadastrosActions.finishSocket())
}

export function* salvarCadastrosSocket() {
    Reactotron.log("salvarCadastrosSocket")
    const realm = yield getRealm()

    // Pega os registro 'pendentes' de alterações
    const colCadastros = realm.objects('Cadastros').filtered(" salvar = true ")

    // Envia o cadastro para base online, um por vez
    yield all(colCadastros.map((cadAtual) => {
        // Define o registro como já salvo
        const data = {
            ...cadAtual,
            salvar: false
        }

        // Salva local
        realm.write(() => {
            realm.create('Cadastros', data, 'modified')
        })

        try {
            return call(api.post, '/cadastros', cadAtual)
        } catch (e) { }
    }))

    yield put(CadastrosActions.finishSocket())
}

export function* excluirCadastrosSocket() {
    Reactotron.log("excluirCadastrosSocket")
    const realm = yield getRealm()
    const colCadastros = realm.objects('Cadastros').filtered(" excluir = true ")

    // Exclui o cadastro da base online, um por vez
    yield all(colCadastros.map((cadAtual) => {
        try {
            return call(api.delete, '/cadastros/' + cadAtual.cpf)
        } catch (e) { }
    }))

    yield put(CadastrosActions.finishSocket())
}