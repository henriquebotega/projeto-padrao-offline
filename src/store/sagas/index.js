import { all, put, spawn, take, takeEvery } from 'redux-saga/effects';
import { eventChannel } from "redux-saga";

import { CadastrosTypes } from '../ducks/cadastros'

import { refreshCadastrosSocket, excluirCadastrosSocket, salvarCadastrosSocket } from './cadastro'

import NetInfo from "@react-native-community/netinfo";
import { OFFLINE, ONLINE } from 'redux-offline-queue';

export function* startWatchingNetworkConnectivity() {
    const channel = eventChannel((emitter) => {
        NetInfo.isConnected.addEventListener('connectionChange', emitter);
        return () => NetInfo.isConnected.removeEventListener('connectionChange', emitter);
    });

    try {
        for (; ;) {
            const isConnected = yield take(channel);

            if (isConnected) {
                yield put({ type: ONLINE });
                yield put({ type: 'REFRESH_CADASTROS_SOCKET' });
            } else {
                yield put({ type: OFFLINE });
            }
        }
    } finally {
        channel.close();
    }
}

export default function* rootSaga() {
    yield all([
        spawn(startWatchingNetworkConnectivity),
        takeEvery(CadastrosTypes.REFRESH_CADASTROS_SOCKET, refreshCadastrosSocket),
        takeEvery(CadastrosTypes.EXCLUIR_CADASTROS_SOCKET, excluirCadastrosSocket),
        takeEvery(CadastrosTypes.SALVAR_CADASTROS_SOCKET, salvarCadastrosSocket),
    ]);
}