import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { offlineMiddleware, suspendSaga, consumeActionMiddleware } from 'redux-offline-queue';
import { REHYDRATE } from 'redux-persist'

import Reactotron from '../config/ReactotronConfig'
import rootReducer from './ducks';
import rootSaga from './sagas';

const sagaMonitor = __DEV__ ? Reactotron.createSagaMonitor() : null;
const sagaMiddleware = createSagaMiddleware({ sagaMonitor });

const middlewares = [];
middlewares.push(offlineMiddleware({ additionalTriggers: REHYDRATE }))
middlewares.push(suspendSaga(sagaMiddleware))
middlewares.push(consumeActionMiddleware())

const composer = __DEV__ ? compose(applyMiddleware(...middlewares), Reactotron.createEnhancer()) : compose(applyMiddleware(...middlewares));
const store = createStore(rootReducer, composer);

sagaMiddleware.run(rootSaga);

export default store;