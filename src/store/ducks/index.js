import { combineReducers } from 'redux';
import { reducer as cadastros } from './cadastros'
import { reducer as offline } from 'redux-offline-queue';

const reducers = combineReducers({
    cadastrosReducer: cadastros,
    offline
});

export default reducers;