import { createReducer, createActions } from 'reduxsauce'
import { markActionsOffline } from "redux-offline-queue";
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
    refreshCadastrosSocket: [''],
    excluirCadastrosSocket: [''],
    salvarCadastrosSocket: [''],
    finishSocket: ['data'],
})

markActionsOffline(Creators, [
    "refreshCadastrosSocket",
    "excluirCadastrosSocket",
    "salvarCadastrosSocket",
    "finishSocket",
]);

export const CadastrosTypes = Types;
export default Creators;

export const INITIAL_STATE = Immutable({
    data: [],
    loading: false,
    refresh: false
})

const refreshCadastrosSocket = (state, action) => {
    return state.merge({ refresh: true });
}

const excluirCadastrosSocket = (state, action) => {
    return state;
}

const salvarCadastrosSocket = (state, action) => {
    return state;
}

const finishSocket = (state, action) => {
    return state.merge({ refresh: false });
}

// const addCadastrosSuccess = (state, action) => {
//     return state.merge({ loading: false }).update('data', data => [...data, action.data])
// }

export const reducer = createReducer(INITIAL_STATE, {
    [Types.REFRESH_CADASTROS_SOCKET]: refreshCadastrosSocket,
    [Types.EXCLUIR_CADASTROS_SOCKET]: excluirCadastrosSocket,
    [Types.SALVAR_CADASTROS_SOCKET]: salvarCadastrosSocket,
    [Types.FINISH_SOCKET]: finishSocket,
})
