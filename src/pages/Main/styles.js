import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        paddingHorizontal: 20,
    },
    welcome: {
        color: '#fff',
        fontSize: 22,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    input: {
        backgroundColor: '#fff',
        borderRadius: 4,
        width: '100%',
        margin: 3,
        padding: 10,
    },
    cad: {
        padding: 10,
        borderRadius: 4,
        margin: 10,
        borderWidth: 1
    }
});

export default styles