import React from 'react';
import { Text, View, TextInput, Button, ImageBackground, TouchableOpacity, FlatList, ToastAndroid } from 'react-native';
import { connect } from "react-redux";
import Reactotron from 'reactotron-react-native';
import uuid from 'react-native-uuid';
import Icon from 'react-native-vector-icons/MaterialIcons'
import NetInfo from "@react-native-community/netinfo";

import { ioURL } from '../../services/api'
import socket from 'socket.io-client'

import CadastrosActions from '../../store/ducks/cadastros'
import styles from './styles'
import getRealm from '~/services/realm';

let io = socket(ioURL)

class Main extends React.Component {

    state = {
        nome: '',
        cpf: '',
        cadastros: []
    }

    componentWillUnmount() {
        if (this.subscription && typeof this.subscription.remove === 'function') {
            this.subscription.remove();
            clearInterval(this.isConnectedFetchInterval);
        }
    }

    isConnectedFetchInterval = () => {
        setInterval(async () => {
            await NetInfo.fetch()
        }, 5000);
    }

    subscription = () => {
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
    }

    handleConnectionChange = (isConnected) => {
        if (isConnected) {
            this.subscribeToEvents();
        }
    }

    async componentDidMount() {
        this.subscription();
        await this.load();
    }

    subscribeToEvents = () => {
        io = socket(ioURL);
        io.off();

        io.on('refreshCadastros', async (ret) => {
            Reactotron.log('refreshCadastros', ret)
            const realm = await getRealm()

            ret.map((reg) => {
                if (reg.registro == "novo") {
                    reg.items.map((item) => {
                        const data = {
                            id: item.id,
                            nome: item.nome,
                            cpf: item.cpf,
                            salvar: false,
                            excluir: false
                        }

                        try {
                            realm.write(() => {
                                try {
                                    realm.create('Cadastros', data, 'modified')
                                } catch (e) {
                                }
                            })
                        } catch (e) {
                        }
                    })
                }

                if (reg.registro == "excluir") {
                    reg.items.map((item) => {
                        const retCadastros = realm.objects('Cadastros').filtered("cpf = '" + item + "'")

                        Reactotron.log('excluirSocket', item, retCadastros)

                        if (retCadastros.length > 0) {
                            try {
                                realm.write(() => {
                                    try {
                                        realm.delete(retCadastros[0])
                                    } catch (e) {
                                    }
                                })
                            } catch (e) {
                            }
                        }
                    })
                }
            })

            await this.load();
        })

        io.on('incluirCadastro', async (ret) => {
            Reactotron.log('incluirCadastro', ret)

            const realm = await getRealm()

            const data = {
                id: uuid.v1(),
                nome: ret.nome,
                cpf: ret.cpf,
                salvar: false,
                excluir: false
            }

            try {
                realm.write(() => {
                    try {
                        realm.create('Cadastros', data, 'modified')
                    } catch (e) {
                    }
                })
            } catch (e) {
            }

            await this.load();
        })

        io.on('excluirCadastro', async (ret) => {
            Reactotron.log('excluirCadastro', ret)

            const realm = await getRealm()
            const retCadastros = realm.objects('Cadastros').filtered("cpf = '" + ret + "'")

            if (retCadastros.length > 0) {
                try {
                    realm.write(() => {
                        try {
                            realm.delete(retCadastros[0])
                        } catch (e) {
                        }
                    })
                } catch (e) {
                }
            }

            await this.load();
        })
    }

    load = async () => {
        const realm = await getRealm()
        const colCadastros = realm.objects('Cadastros').sorted('id', true).filtered(" excluir = false ");

        this.setState({ cadastros: colCadastros })
    }

    salvar = async () => {
        const realm = await getRealm()

        const data = {
            id: uuid.v1(),
            nome: this.state.nome,
            cpf: this.state.cpf,
            salvar: true,
            excluir: false
        }

        // Salva local
        try {
            realm.write(() => {
                try {
                    realm.create('Cadastros', data, 'modified')
                } catch (e) {
                }
            })
        } catch (e) {
        }

        this.setState({
            nome: '',
            cpf: ''
        })

        // Faz chamada para salvar os itens (pendentes)
        this.props.salvarCadastrosSocket();

        await this.load();
    }

    handleExcluir = async (cadAtual) => {
        const realm = await getRealm()

        const retCadastros = realm.objects('Cadastros').filtered("cpf = '" + cadAtual.cpf + "'")

        if (retCadastros.length > 0) {
            const data = {
                ...retCadastros[0],
                salvar: false,
                excluir: true
            }

            // Salva local
            try {
                realm.write(() => {
                    try {
                        realm.create('Cadastros', data, 'modified')
                    } catch (e) {
                    }
                })
            } catch (e) {
            }
        }

        // Faz chamada para remover os itens excluidos (pendentes)
        this.props.excluirCadastrosSocket();

        await this.load();
    }

    renderItem = (cadAtual) => {
        return (
            <View key={cadAtual.id} style={[styles.cad, { flex: 1, flexDirection: 'column', marginTop: 10, justifyContent: 'space-between' }]}>
                <Text>Salvar: {JSON.stringify(cadAtual.salvar)}, Excluir: {JSON.stringify(cadAtual.excluir)}</Text>
                <Text>{cadAtual.cpf} - {JSON.stringify(cadAtual.dataCadastro)}</Text>

                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => this.handleExcluir(cadAtual)}>
                        <Icon name="delete" size={24} color="white" />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    render() {
        return (
            <ImageBackground source={{ uri: 'https://s3-sa-east-1.amazonaws.com/rocketseat-cdn/background.png', }} style={styles.container} resizeMode="cover">

                <TextInput style={styles.input} onChangeText={t => this.setState({ nome: t })} value={this.state.nome} autoCorrect={false} placeholder="Nome" />
                <TextInput style={styles.input} onChangeText={t => this.setState({ cpf: t })} value={this.state.cpf} autoCorrect={false} placeholder="CPF" />

                <Button title="Salvar" onPress={() => this.salvar()} />
                <Button title="Atualizar" onPress={() => this.props.refreshCadastrosSocket()} />

                <FlatList data={this.state.cadastros} keyExtractor={(t) => t._id} renderItem={({ item }) => this.renderItem(item)} />

            </ImageBackground>
        )
    }
}

const mapStateToProps = state => ({
    cadastros: state.cadastrosReducer
})

const mapDispatchToProps = dispatch => ({
    refreshCadastrosSocket: () => dispatch(CadastrosActions.refreshCadastrosSocket()),
    salvarCadastrosSocket: () => dispatch(CadastrosActions.salvarCadastrosSocket()),
    excluirCadastrosSocket: () => dispatch(CadastrosActions.excluirCadastrosSocket()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Main)